name 'consul'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'MIT'
description 'Installs/Configures consul'
long_description 'Installs/Configures consul'
version '0.1.1'
chef_version '>= 12.14' if respond_to?(:chef_version)
issues_url 'https://gitlab.com/julienlevasseur/chef-consul/issues'
source_url 'https://gitlab.com/julienlevasseur/chef-consul'

depends 'util-consul'
depends 'zipfile'

supports 'ubuntu'
supports 'centos'
supports 'debian'
