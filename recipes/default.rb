#
# Cookbook:: consul
# Recipe:: default
#

# directory node['consul']['install_dir'] do
#   recursive true
# end
# 
directory node['consul']['config_dir'] do
  recursive true
end
# 
# directory node['consul']['data_dir'] do
#   recursive true
# end
# 
# remote_file "#{node['consul']['install_dir']}/consul.zip" do
#   source "https://releases.hashicorp.com/consul/#{node['consul']['version']}/consul_#{node['consul']['version']}_linux_amd64.zip"
#   mode '0644'
#   not_if { ::File.exist?("#{node['consul']['install_dir']}/consul.zip") }
#   not_if { ::File.exist?("#{node['consul']['install_dir']}/consul") }
# end
# 
# zipfile "#{node['consul']['install_dir']}/consul.zip" do
#   into "#{node['consul']['install_dir']}/"
#   only_if { ::File.exist?("#{node['consul']['install_dir']}/consul.zip") }
# end
# 
# file "#{node['consul']['install_dir']}/consul.zip" do
#   action :delete
# end
# 
# require 'json'

consul node['consul']['version'] do
  platform node['platform']
end

pretty_settings = Chef::JSONCompat.to_json(node['consul']['config'])

template "#{node['consul']['config_dir']}/config.json" do
  source 'config.json.erb'
  owner node['consul']['owner']
  group node['consul']['group']
  mode '0644'
  variables settings: pretty_settings
end

#if node['consul']['acl']['enabled'] == true
#  file "#{node['consul']['config_dir']}/config.json" do
#    content node['consul']['acl']['config'].to_json
#    mode '0644'
#    owner 'root'
#    group 'root'
#  end
#end

if node['platform'] == 'ubuntu' || node['platform'] == 'debian'
  if ::File.exists?('/bin/systemd')
    template '/lib/systemd/system/consul.service' do
      source 'consul.service.erb'
      owner node['consul']['owner']
      group node['consul']['group']
      mode '0644'
    end

    service 'consul' do
      action [:enable, :start]
      not_if { Chef::Config[:file_cache_path].include?('kitchen') }
    end
  else
    template '/etc/init.d/consul' do
      source 'consul-init.erb'
      owner node['consul']['owner']
      group node['consul']['group']
      mode '0755'
    end

    execute 'enable-consul-service' do
      command '/etc/init.d/consul start'
      not_if 'ps -aef|grep consul|grep -v grep'
      not_if { Chef::Config[:file_cache_path].include?('kitchen') }
    end
  end
elsif node['platform'] == 'darwin'
  launchd = node['consul']['systemctl']['ExecStart'].split(' ')

  template '/System/Library/LaunchDaemons/consul.plist' do
    source 'consul.plist.erb'
    owner node['consul']['owner']
    group node['consul']['group']
    mode '0644'
    variables Program: launchd.shift, ProgramArguments: launchd
    notifies :restart, 'service[consul]', :delayed
  end
end

execute 'enable-consul-service' do
  command "#{node['consul']['service']['ExecStart']} &"
  not_if 'ps -aef|grep consul|grep -v grep'
  only_if { Chef::Config[:file_cache_path].include?('kitchen') }
end

# node['consul']['acls'].each do |acl|
#   consul_acl acl['Name'] do
#     value acl
#   end
# end

chef_gem 'diplomat' do
  version node['consul']['diplomat']['version']
  action :install
end

Diplomat.configure do |config|
  config.url = node['consul']['url']
  config.acl_token = node['consul']['acl_token']
  config.options = { request: { timeout: 10 } }
end

#if Chef::Config[:file_cache_path].include?('kitchen')
#  node.save # ~FC075
#  file '/tmp/kitchen/nodes/node.json' do
#    owner 'root'
#    group 'root'
#    mode 0755
#    content ::File.open("/tmp/kitchen/nodes/#{node.name}.json").read
#    action :create
#    only_if { ::File.exist?("/tmp/kitchen/nodes/#{node.name}.json") }
#  end
#end
