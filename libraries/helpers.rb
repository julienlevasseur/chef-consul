#class Chef
#  class Recipe
#    module Consul
#      def self.is_installed(install_dir)
#        return ::File.file?("#{install_dir}/consul")
#      end
#
#      def self.consul_installed_version(install_dir)
#        require 'open3'
#        stdout, stderr, status = Open3.capture3(
#          "#{install_dir}/consul -v|head -1|awk '{print $2}'|sed -e 's/v//g'"
#        )[0]
#        return stdout.sub!("\n", "")
#      end
#      
#      def self.cleanup(install_dir)
#        ::File.delete("#{install_dir}/consul")
#      end
#      
#      def self.install_consul(version, install_dir)
#        begin
#          unless Dir.exist?(install_dir)
#            ::Chef.Log.info("[DEBUG] create install_dir")
#            require 'fileutils'
#            FileUtils.mkdir_p "#{install_dir}"
#          end
#
#          ::Chef.Log.info("[DEBUG] download remote file")
#          remote_file "/tmp/consul_#{version}_linux_amd64.zip" do
#            source "https://releases.hashicorp.com/consul/#{version}/consul_#{version}_linux_amd64.zip"
#            mode '0644'
#            action :create
#          end
#          
#          ::Chef.Log.info("[DEBUG] unzip archive")
#          zipfile "/tmp/consul_#{version}_linux_amd64.zip" do
#            into install_dir
#          end
#          Chef::Log.debug("Installing Consul version: #{version}")
#        rescue
#          Chef::Log.error("Failed to install Consul #{version}")
#        end
#      end
#    end
#  end
#end
