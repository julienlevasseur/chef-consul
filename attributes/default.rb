
default['consul']['version'] = '1.7.1'
default['consul']['diplomat']['version'] = '2.0.2'

default['consul']['url'] = 'http://localhost:8500'

default['consul']['install_dir'] = '/usr/local/bin'
default['consul']['config_dir']  = '/etc/consul'
default['consul']['data_dir']    = '/var/lib/consul'
default['consul']['log_dir']     = '/var/log/consul'
default['consul']['bind_addr']   = '0.0.0.0'

if node['platform'] == 'ubuntu'
  default['consul']['owner'] = 'root'
  default['consul']['group'] = 'root'
elsif node['platform'] == 'mac_os_x'
  default['consul']['owner'] = 'root'
  default['consul']['group'] = 'sys'
end

default['consul']['config'] = {}

default['consul']['datacenter'] = nil
default['consul']['acl_token']  = nil

default['consul']['service']['ExecStart'] = "#{node['consul']['install_dir']}/consul agent -server -client=0.0.0.0 -bootstrap-expect=1 -ui -dev -config-dir #{node['consul']['config_dir']}"

default['consul']['acl']['enabled'] = false
default['consul']['acl']['config'] = {
  acl_default_policy: 'deny',
  acl_down_policy: 'deny',
}

default['consul']['acls'] = [
  {
    Name: 'terraform',
    Type: "client",
    Rules: "key \"tfstate\" { \"policy\" = \"write\" } session \"\" { \"policy\" = \"write\" }"
  }
]
