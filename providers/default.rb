use_inline_resources

def whyrun_supported
  true
end

def consul_is_installed
  return ::File.file?("#{new_resource.install_dir}/consul")
end

def consul_installed_version
  require 'open3'
  stdout, stderr, status = Open3.capture3(
    "#{new_resource.install_dir}/consul -v|head -1|awk '{print $2}'|sed -e 's/v//g'"
  )[0]
  return stdout.sub!("\n", "")
end

def cleanup
  ::File.delete("#{new_resource.install_dir}/consul")
end


def download_archive
  if new_resource.platform == 'ubuntu' || new_resource.platform == 'centos'
    remote_file "/tmp/consul_#{new_resource.version}_linux_amd64.zip" do
      source "https://releases.hashicorp.com/consul/#{new_resource.version}/consul_#{new_resource.version}_linux_amd64.zip"
      mode '0644'
      action :create
    end
  elsif new_resource.platform == 'mac_os_x'
    remote_file "/tmp/consul_#{new_resource.version}_darwin_amd64.zip" do
      source "https://releases.hashicorp.com/consul/#{new_resource.version}/consul_#{new_resource.version}_darwin_amd64.zip"
      mode '0644'
      action :create
    end
  end
end

def unzip_archive
  if new_resource.platform == 'ubuntu' || new_resource.platform == 'centos'
    zipfile "/tmp/consul_#{new_resource.version}_linux_amd64.zip" do
      into new_resource.install_dir
    end
  elsif new_resource.platform == 'mac_os_x'
    zipfile "/tmp/consul_#{new_resource.version}_darwin_amd64.zip" do
      into new_resource.install_dir
    end
  end
end

action :install do
  Chef::Log.info("[DEBUG] in :install")
  unless consul_is_installed
    converge_by "Installing Consul '#{new_resource.version}'" do
      begin
        unless Dir.exist?(new_resource.install_dir)
          ::Chef.Log.info("[DEBUG] create install_dir")
          require 'fileutils'
          FileUtils.mkdir_p "#{new_resource.install_dir}"
        end
    
        download_archive
        unzip_archive
        Chef::Log.debug("Installing Consul #{new_resource.version}")
      rescue
        Chef::Log.error("Failed to install Consul #{new_resource.version}")
      end
    end
  else
    if consul_installed_version < new_resource.version
      converge_by "Upgrading Consul to '#{new_resource.version}'" do
        begin
          cleanup
          download_archive
          unzip_archive
          Chef::Log.debug("Upgrading Consul version: from #{consul_installed_version} to #{new_resource.version}")
        rescue
          Chef::Log.error("Failed to update Consul from #{consul_installed_version} to #{new_resource.version}")
        end
      end
    end
  end
end

action :uninstall do
  if consul_is_installed
    converge_by "Uninstalling Consul" do
      begin
        cleanup
        Chef::Log.debug("Uninstalling Consul")
      rescue
        Chef::Log.error("Failed to uninstall Consul")
      end
    end
  end
end