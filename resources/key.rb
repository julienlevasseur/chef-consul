resource_name :consul_key
provides :consul_key

property :key, String, name_property: true
property :value, String

default_action :create_or_update

action :create_or_update do
  unless ::Chef::Recipe::Consul::Key.exist?(new_resource.key)
    converge_by "Creating / Updating Consul Key '#{new_resource.key}'" do
      begin
        ::Chef::Recipe::Consul::Key.put(new_resource.key, new_resource.value)
        Chef::Log.debug("Creating / Updating Consul Key [#{new_resource.key}] with value: #{new_resource.value}")
      rescue LoadError
        Chef::Log.error("Failed to create or update Consul Key #{new_resource.key}")
      end
    end
  end
end

action :destroy do
  if ::Chef::Recipe::Consul::Key.exist?(new_resource.key)
    converge_by "Deleting Consul Key '#{new_resource.key}'" do
      begin
        ::Chef::Recipe::Consul::Key.destroy(new_resource.key)
        Chef::Log.debug("Deleting Consul Key [#{new_resource.key}]")
      rescue LoadError
        Chef::Log.error("Failed to delete Consul Key #{new_resource.key}")
      end
    end
  end
end
