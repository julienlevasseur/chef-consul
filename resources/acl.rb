resource_name :consul_acl
provides :consul_acl

property :id, String, name_property: true
property :value, Hash

default_action :create_or_update

action :create_or_update do
  unless ::Chef::Recipe::Consul::Acl.exist?(new_resource.id)
    converge_by "Creating / Updating Consul Acl '#{new_resource.acl}'" do
      begin
        ::Chef::Recipe::Consul::Acl.update(new_resource.value)
        Chef::Log.debug("Creating / Updating Consul Acl [#{new_resource.acl}] with value: #{new_resource.value}")
      rescue LoadError
        Chef::Log.error("Failed to create or update Consul acl #{new_resource.acl}")
      end
    end
  end
end

action :destroy do
  if ::Chef::Recipe::Consul::Acl.exist?(new_resource.id)
    converge_by "Deleting Consul Acl '#{new_resource.acl}'" do
      begin
        ::Chef::Recipe::Consul::Acl.destroy(new_resource.id)
        Chef::Log.debug("Deleting Consul Acl [#{new_resource.acl}]")
      rescue LoadError
        Chef::Log.error("Failed to delete Consul Acl #{new_resource.acl}")
      end
    end
  end
end
